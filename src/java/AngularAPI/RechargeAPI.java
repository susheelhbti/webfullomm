/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AngularAPI;

import com.login.util.Util;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mala singh
 */
public class RechargeAPI extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //response.setContentType("text/html;charset=UTF-8");
        Connection con = null;
		Statement st = null;
		  PrintWriter out=response.getWriter();
		try {
                 
			con = Util.getConnection();
			st = con.createStatement();
			String delimit = String.valueOf(request.getParameter("delimit"));
			String status_index_number = String.valueOf(request.getParameter("status_index_number"));
			String orderid_index_number = String.valueOf(request.getParameter("orderid_index_number"));
			String operatorid_index_number =String.valueOf(request.getParameter("operatorid_index_number"));
                        String success_url =String.valueOf(request.getParameter("success_url"));
                        String failure_url =String.valueOf(request.getParameter("failure_url"));
                        String pending_url =String.valueOf(request.getParameter("pending_url"));
			
		 

			
	 PreparedStatement stmt = con.prepareStatement("insert into recharge(delimit,Status_index_number,orderid_index_number,operatorid_index_number,Success_url,failure_url,pending_url ) values (?,?,?,?,?,?,?)");
		        stmt.setString(1, delimit);
                        stmt.setString(2, status_index_number);  //dr 
                        stmt.setString(3,orderid_index_number);  //cr 
                        stmt.setString(4,operatorid_index_number);
                        stmt.setString(5,success_url);
                        stmt.setString(6,failure_url);
                        stmt.setString(7, pending_url);
                      int i = stmt.executeUpdate();
                     out.println(i+"submitted");
                                      
                                           
                }catch (Exception e) {
			
			 out.println("error");
			
		} finally { 
			try {
				if(st!=null)
					st.close();
				if(con!=null)
					con.close();
			} catch (SQLException e) {
			} 
		} 
        
       
    }


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
