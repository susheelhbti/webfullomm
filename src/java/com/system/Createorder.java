/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system;

import com.function.Send;
import com.login.util.Util;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jyotiserver2010
 */
public class Createorder extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        try {
            PrintWriter out = response.getWriter();
            String client = request.getParameter("client").trim();
             String host = request.getParameter("host").trim();
            String idd = request.getParameter("id");
            String type = request.getParameter("case");
            System.out.println(request.getParameter("total_bitcoin"));
            System.out.println(48);
            System.out.println(request.getParameter("max_limit"));
          ///  String type = request.getParameter("case");
            int total_bitcoin = Integer.parseInt(request.getParameter("total_bitcoin"));
            double price = Double.parseDouble(request.getParameter("price"));
            System.out.print(price);
            int max_limit = Integer.parseInt(request.getParameter("max_limit"));
if(total_bitcoin < max_limit){
    Connection con = null;
    Statement st = null;
    String id = null;
    //out.print(username);
    System.out.println(1111);
    
    con = Util.getConnection();
    st = con.createStatement();
    String query = "insert into bitorder(client,type,status,trade_id,total_bitcoin,host,amount_in_usd) values ('" + client + "','" + type + "','Started','" + idd + "','" + total_bitcoin + "','"+host+"','"+price+"')";
    System.out.println(query);
    int i = st.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
    ResultSet rs = st.getGeneratedKeys();
    if (rs.next()) {
        id = rs.getString(1);
    }
    if (i > 0) {
        Send s=new Send();
        String p=s.sendbit(client,price); System.out.println(p);
        request.setAttribute("msg", "order created successfully....");
        out.println("{\"Error\": \"False\" ,\"Message\": \"order created successfully....\"}");
        // System.out.println("{\"Error\": \""+e+"\" ,\"Message\": \""+msg+"'\" ,\"Avtar\": \"http://d3k0efmm336514.cloudfront.net/lordkrish_63ba1e1f67bfa2345dc3f072b4a386e2.jpeg\"}");
        RequestDispatcher rd1 = request.getRequestDispatcher("chatbox.jsp?id=" + id);
        rd1.forward(request, response);
        
    }
} else {
    request.setAttribute("msg", "limit exceededed....");
     RequestDispatcher rd1 = request.getRequestDispatcher("message.jsp");
        rd1.forward(request, response);
}
        }
catch (Exception ex) {
            request.setAttribute("msg", "Error....");
            out.println("{\"Error\": \"False\" ,\"Message\": \"\"Error....\"\"");
            Logger.getLogger(Createorder.class.getName()).log(Level.SEVERE, null, ex);
        }
        RequestDispatcher rd1 = request.getRequestDispatcher("message.jsp");
        rd1.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
