/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.login.util.Util;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author fullomm
 */
public class allUser extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         response.setContentType("application/json;charset=UTF-8");
         PrintWriter out = response.getWriter();
        Connection con = null;
        Statement st = null;
        ResultSet rs;
       
        try  {
           con = Util.getConnection();
            st = con.createStatement();
             PreparedStatement stmt = con.prepareStatement("select * from register");
        rs = stmt.executeQuery();
            // System.out.println(51);
            ArrayList<user> a = new ArrayList<>();
            while (rs.next()) {
                user a1 = new user();

                a1.setid(rs.getString("id"));
                a1.setname(rs.getString("name")); 
                
                a1.setemail(rs.getString("email"));
                a1.setmobile(rs.getString("mobile"));
                a1.setstatus(rs.getString("status"));
                a.add(a1);
                 System.out.println("submitted");
            }
                   Gson gson=new GsonBuilder().create();
    String jsonArray=gson.toJson(a);
            //out.println(messages); 
            out.write(jsonArray);

            //["Hi","Hello","How r u?"]
         //   out.println(messages);
            con.close();
        } 
            catch (Exception e) {
            out.println(e);
        }
    
    
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
private class user {

        public String a;
        public String id;
        public String name;
        public String api;
        public String email;
        public String mobile;
        public String status;
       
        public void setid(String n) {
            id = n;

        }

        public void setname(String n) {
            name = n;

        }
 public void setapi(String n) {
           api = n;

        }
  public void setemail(String n) {
             email = n;

        }
   public void setmobile(String n) {
             mobile = n;

        }
    public void setstatus(String n) {
             status = n;

        }

}}