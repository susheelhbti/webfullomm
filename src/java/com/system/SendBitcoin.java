/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system;

import com.login.util.Util;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author jyotiserver2010
 */
public class SendBitcoin extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
        try {
             HttpSession session = request.getSession();
            String username = (String) session.getAttribute("username");
            String address_to = String.valueOf(request.getParameter("address_to"));
            String amount = String.valueOf(request.getParameter("amount"));
            long fees = 37385;//String.valueOf(request.getParameter("fees"));
            String description = String.valueOf(request.getParameter("description"));
           // String from = String.valueOf(request.getParameter("from"));
             String bitid=null;
             String bitaddress=null;
            String password=null;
            String msg=null;
            String e= null;
            Connection con = null;
            Statement st = null;
             System.out.println("Starting");
            con = Util.getConnection();
            st = con.createStatement();
            ///1 Satoshi	= 0.00000001 ฿
           double am=   Double.parseDouble(amount);//Float.parseFloat(amount);
           double amount_in_satoshi=am*100000000;
           System.out.println("amount_in_btc"+amount);
           System.out.println("amount_in_satoshi"+amount_in_satoshi);
		String query = "select * from register  where    email='" + username + "' ";
            System.out.println(query);
            ResultSet rs = st.executeQuery(query);
            if(rs.next()){
             password=rs.getString("password");
             bitid=rs.getString("bitid");
             //bitaddress=rs.getString(4);
             System.out.println(rs.getString(4));
           // String password=rs.getString("password");
            } 
           String u;
           System.out.println("url calling");
         //    u="";
         //    u ="http://127.0.0.1:3000/merchant/"+bitid+"/payment?password="+password+"&to="+address_to+"&amount="+amount+"&from="+bitaddress+"&fee="+fees+"" ;
          // u="http://127.0.0.1:3000/merchant/"+bitid+"/payment?password="+password+"&to="+address_to+"&amount="+amount_in_satoshi+"&from="+from+"&fee="+fees+"";
                    u="http://127.0.0.1:3000/merchant/"+bitid+"/payment?password="+password+"&to="+address_to+"&amount="+amount_in_satoshi+"&fee="+fees+"";
  
           System.out.println(u);
            
                
           String output=wget(u);    
           JSONObject jsonObj = new JSONObject(output);
           ///String description="send ";
           transactions t =new transactions();

                  String a=t.tran(username ,address_to,bitaddress,amount,amount_in_satoshi,fees,jsonObj.getString("tx_hash"),jsonObj.getString("message"),description);

 //String a=output;// t.tran(username ,address_to,bitaddress,jsonObj.getString("tx_hash"),jsonObj.getString("message"),description);
          // String a=t.tran(username ,address_to,bitaddress,amount,amount_in_satoshi,fees,"asd1xz","d1111cx",description);
request.setAttribute("msg", a);
/*String msg5="<h3>url='"+u+"'</h3>"
        + "<h3>output='"+output+"'</h3>";
String subject="wallet info";

sms s=new sms();
s.sendemailtoself(msg5,subject);*/
                      // System.out.print(jsonObj.getString("error")) ;
        
            
        } catch (Exception ex) {
             request.setAttribute("msg", "Error in transfer,please try Again....");
            Logger.getLogger(SendBitcoin.class.getName()).log(Level.SEVERE, null, ex);
        }
        RequestDispatcher rq = request.getRequestDispatcher("message.jsp");

                        rq.forward(request, response);
    }
    
      public String wget(String u)
            throws MalformedURLException, ProtocolException, SQLException, JSONException, IOException {

        URL url = new URL(u);
        java.net.HttpURLConnection conn = (java.net.HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "application/json");

        if (conn.getResponseCode() != 200) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + conn.getResponseCode());
        }

        BufferedReader br = new BufferedReader(new InputStreamReader(
                (conn.getInputStream())));

        return br.readLine();
    }


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
