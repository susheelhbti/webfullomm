/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.escape.Escaper;
import com.google.common.io.BaseEncoding;
import com.google.common.net.UrlEscapers;
import com.google.common.primitives.Ints;
import com.login.util.Util;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpSession;


/**
 *
 * @author fullomm
 */
public class SetGoogleAuth extends HttpServlet {

     private static final int SCRET_BYTE = 10;
    private static final int SCRATCH_CODES = 5;
    private static final int BYTES_PER_SCRATCH_CODE = 4;

    private static final int TIME_PRECISION = 3;
    private static final String CRYPTO_ALGO = "HmacSHA1";

    private static final String URL_FORMAT = "https://chart.googleapis.com/chart?chs=430x430&chld=M%%7C0&cht=qr&chl="
            + "otpauth://totp/"
            + "%s@%s%%3Fsecret%%3D%s";

    public static String generateSecretKey() {
        // Allocating the buffer
        byte[] buffer = new byte[SCRET_BYTE + SCRATCH_CODES * BYTES_PER_SCRATCH_CODE];

        // Filling the buffer with random numbers.
        // Notice: you want to reuse the same random generator
        // while generating larger random number sequences.
        new SecureRandom().nextBytes(buffer);

        // Getting the key and converting it to Base32
        byte[] secretKey = Arrays.copyOf(buffer, SCRET_BYTE);
        return BaseEncoding.base32().encode(secretKey);
    }

    public static String getQRBarcodeURL(String user, String host, String secret) {
        Escaper urlEscaper = UrlEscapers.urlFragmentEscaper();
        return String.format(URL_FORMAT, urlEscaper.escape(user), urlEscaper.escape(host), secret);
    }

    public static boolean checkPassword(String secretKey, String userInput)
            throws NoSuchAlgorithmException, InvalidKeyException {
        Integer code = Ints.tryParse(userInput);
        if (code == null) {
            //code is not an integer
            return false;
        }

        long currentTime = Calendar.getInstance().getTimeInMillis() / TimeUnit.SECONDS.toMillis(30);
        return check_code(secretKey, code, currentTime);
    }

    private static boolean check_code(String secret, long code, long t)
            throws NoSuchAlgorithmException, InvalidKeyException {
        byte[] decodedKey = BaseEncoding.base32().decode(secret);

        // Window is used to check codes generated in the near past.
        // You can use this value to tune how far you're willing to go.
        int window = TIME_PRECISION;
        for (int i = -window; i <= window; ++i) {
            long hash = verify_code(decodedKey, t + i);

            if (hash == code) {
                return true;
            }
        }

        // The validation code is invalid.
        return false;
    }

    private static int verify_code(byte[] key, long t) throws NoSuchAlgorithmException, InvalidKeyException {
        byte[] data = new byte[8];
        long value = t;
        for (int i = 8; i-- > 0; value >>>= 8) {
            data[i] = (byte) value;
        }

        SecretKeySpec signKey = new SecretKeySpec(key, CRYPTO_ALGO);
        Mac mac = Mac.getInstance(CRYPTO_ALGO);
        mac.init(signKey);
        byte[] hash = mac.doFinal(data);

        int offset = hash[20 - 1] & 0xF;

        // We're using a long because Java hasn't got unsigned int.
        long truncatedHash = 0;
        for (int i = 0; i < 4; ++i) {
            truncatedHash <<= 8;
            // We are dealing with signed bytes:
            // we just keep the first byte.
            truncatedHash |= (hash[offset + i] & 0xFF);
        }

        truncatedHash &= 0x7FFF_FFFF;
        truncatedHash %= 1_000_000;

        return (int) truncatedHash;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            response.setContentType("text/html;charset=UTF-8");
            Connection con = null;
            Statement st = null;
            
            
            con = Util.getConnection();
            st = con.createStatement();
            String secret=request.getParameter("secret");
            String otp=request.getParameter("otp");
            //PrintWriter out = response.getWriter();
            //out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">");
            //out.println("<html>");
            //out.println("<head bgcolor=\"#004A7C\">");
            //out.println("<title> QR Code</title>");            
            //out.println("</head>");
            //out.println("<body bgcolor=\"#444\">"); out.println("<h1>Scan QR Code</h1>");
           // String secret = generateSecretKey();

            //out.println("<div style=\"margin-left:300px;\"><img src='" + getQRBarcodeURL("user", "host", secret) + "' />");

            HttpSession session = request.getSession();
            String username = String.valueOf(session.getAttribute("username"));
            boolean  b = checkPassword(secret,otp);
                if(b==true){
            String query = "update register set secret_key='" + secret + "'  , google_auth_status='Enable'    where username='" + username + "'  ";
            System.out.println(query);
            int i = st.executeUpdate(query);
            if (i > 0) {
                request.setAttribute("msg", " updated Successfully!!");
                RequestDispatcher rq = request.getRequestDispatcher("profile.jsp");
                rq.forward(request, response);
            } else {
                request.setAttribute("msg", "Your Details  are Invalid!!");
                RequestDispatcher rq = request.getRequestDispatcher("profile.jsp");
                rq.forward(request, response);
            }
                } else {
                request.setAttribute("msg", "Your Otp  are Invalid!!");
                RequestDispatcher rq = request.getRequestDispatcher("profile.jsp");
                rq.forward(request, response);
            }
            //out.println("</br><h3>"+secret+"<h3></div>");
        } catch (Exception ex) {
            Logger.getLogger(SetGoogleAuth.class.getName()).log(Level.SEVERE, null, ex);
        }

        
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
