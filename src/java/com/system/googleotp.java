/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system;

import com.login.util.Util;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fullomm
 */
public class googleotp extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      try {
            Connection con = null;
            Statement st = null;
            
            
            con = Util.getConnection();
            st = con.createStatement();
   
            
            String otp = request.getParameter("otp");
            String username = request.getParameter("username");
           
           
            
            String query = "select  * from register where username='" + username + "'  ";
           System.out.println(query);
            ResultSet rs = st.executeQuery(query);
            if (rs.next()) {
                 //byte[] bytes = rs.getString("secret_key").getBytes();
                SetGoogleAuth s=new SetGoogleAuth();
              
                
              boolean  b = s.checkPassword(rs.getString("secret_key"),otp);
                if(b==true){
              HttpSession session = request.getSession();
                session.setAttribute("username", rs.getString("username"));
                session.setAttribute("email", rs.getString("email"));
                session.setAttribute("roll", rs.getString("roll"));
                session.setAttribute("parent", rs.getString("parent"));
                //session.setAttribute("call_back_url",call_back_url);
                 response.sendRedirect("profile.jsp");
            } else {
                request.setAttribute("msg", "Your Details  are Invalid!!");
                RequestDispatcher rq = request.getRequestDispatcher("profile.jsp");
                rq.forward(request, response);
            }}
            else {
                request.setAttribute("msg", "Your Details  are Invalid!!");
                RequestDispatcher rq = request.getRequestDispatcher("profile.jsp");
                rq.forward(request, response);
            }
        } catch (Exception ex) {
            Logger.getLogger(googleotp.class.getName()).log(Level.SEVERE, null, ex);
        }
            }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
