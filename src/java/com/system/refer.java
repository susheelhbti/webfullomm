/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system;

import com.login.util.Util;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 *
 * @author jyotiserver2010
 */
public class refer extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         response.setContentType("text/html;charset=UTF-8");
      PrintWriter out = response.getWriter();
            /* TODO output your page here. You may use following sample code. */
           
      Connection con = null;
		Statement st = null;
		 
		try {
                  
                    
			con = Util.getConnection();
			st = con.createStatement();
			String name = String.valueOf(request.getParameter("name"));
			String email = String.valueOf(request.getParameter("email"));
			 HttpSession session = request.getSession();
        String username = String.valueOf(session.getAttribute("username")).trim();
		 
        
        
     String msg  = "<p>Your friend "+username+" has invited you to register at papafast.com.</p> <p>To register kindly visit <a href="+"http://papafast.com:8080/papafast/register.jsp?reference="+username+">Click Here</a></p>\n  <p>Enjoy!!</p>" ;   // s.smssend(msg,mobile);
                
          
          
          
          sms s=new sms();
                        // s.smssend(msg,mobile);
                
                s.sendemail(  msg,  email,   "subject");
                
                
    
                 request.setAttribute("msg", "Request send....");
					
                            RequestDispatcher rd1 = request.getRequestDispatcher("message.jsp");
					rd1.forward(request, response);
        
                                           
                }catch (Exception e) {
			
			 System.out.println("error");
			
		} finally { 
			try {
				if(st!=null)
					st.close();
				if(con!=null)
					con.close();
			} catch (SQLException e) {
			} 
		} 
		
		 RequestDispatcher rd = request.getRequestDispatcher("refer.jsp");
		rd.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
