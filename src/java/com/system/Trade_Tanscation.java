/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system;

import com.login.util.Util;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author jyotiserver2010
 */
public class Trade_Tanscation extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         HttpSession session = request.getSession();
            String username = String.valueOf(session.getAttribute("username"));
        String type = String.valueOf(request.getParameter("type"));
        String location = String.valueOf(request.getParameter("location"));
        String currency = String.valueOf(request.getParameter("currency"));
        String margin = String.valueOf(request.getParameter("margin"));
        String payment_method = String.valueOf(request.getParameter("payment_method"));
        String price_equation = String.valueOf(request.getParameter("price_equation"));
        String min_tranaction = String.valueOf(request.getParameter("min_tranaction"));
        String max_tranaction = String.valueOf(request.getParameter("max_tranaction"));
        String restrict_amount = String.valueOf(request.getParameter("restrict_amount"));
        //String opening_hours = String.valueOf(request.getParameter("opening_hours"));
        //String closing_hours = String.valueOf(request.getParameter("closing_hours"));
        String terms_of_trade = String.valueOf(request.getParameter("terms_of_trade"));
        String track_liquidity = String.valueOf(request.getParameter("track_liquidity"));
        String identified_person_only = String.valueOf(request.getParameter("identified_person_only"));
        String sms_verification = String.valueOf(request.getParameter("sms_verification"));
        String trusted_person_only = String.valueOf(request.getParameter("trusted_person_only"));

        try {
            ResultSet rs;
            int i = 0;
            Connection con = Util.getConnection();
            Statement st = con.createStatement();
            String q = "INSERT INTO `trade_transaction`(`username`,`type`, `location`, `currency`, `margin`, `price_equation`,"
                    + " `min_transaction`, `max_transcation`, `restrict_amount`, `terms_of_trade`, `track_liquidity`,"
                    + " `sms_verifiation`, `trusted_person_only`,`payment_method`,`identified_person_only`) VALUES ('" + username + "','" + type + "',"
                    + "'" + location + "','" + currency + "','" + margin + "','" + price_equation + "','" + min_tranaction + "','" + max_tranaction + "',"
                    + "'" + restrict_amount + "','" + terms_of_trade + "','" + track_liquidity + "',"
                    + "'" + sms_verification + "','" + trusted_person_only + "','" + payment_method + "','"+identified_person_only+"') ";
            System.out.println(q);
            try {
                i = st.executeUpdate(q, Statement.RETURN_GENERATED_KEYS);

            } catch (Exception e1) {
                String message = e1.getMessage();
                System.out.println(message);
            }
            System.out.println(59);

            // PrintWriter out=response.getWriter();
            int insertID = 0;
            rs = st.getGeneratedKeys();
            if (rs.next()) {
                insertID = rs.getInt(1);
            }
            System.out.println(insertID);

            if (i > 0) {
                String aa = "Successfully Executed";
                 request.setAttribute("msg", "Successfully Executed....");
                System.out.print(11);
                
            } else {
                String aa = "Failed";
                System.out.print(12);
                request.setAttribute("msg", "Failed,Try Again....");
               
            }
        } catch (Exception ex) {
            Logger.getLogger(Trade_Tanscation.class.getName()).log(Level.SEVERE, null, ex);
        }
        RequestDispatcher rd1 = request.getRequestDispatcher("message.jsp");
            rd1.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
