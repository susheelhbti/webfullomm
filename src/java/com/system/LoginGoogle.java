/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson.JacksonFactory; 
import com.login.util.Util;
import java.io.IOException;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.Statement;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fullomm
 */
public class LoginGoogle extends HttpServlet {
        HttpTransport transport = new NetHttpTransport();

    JsonFactory jsonFactory = new JacksonFactory();
    GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(transport, jsonFactory)
            .setAudience(Collections.singletonList("201115857082-cs8ia562e467ro22njp70id9surv5afo.apps.googleusercontent.com"))
            // Or, if multiple clients access the backend:
            //.setAudience(Arrays.asList(CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3))
            .setIssuer("https://accounts.google.com")
            .build();
    
    GoogleIdTokenVerifier verifier2 = new GoogleIdTokenVerifier.Builder(transport, jsonFactory)
            .setAudience(Collections.singletonList("201115857082-cs8ia562e467ro22njp70id9surv5afo.apps.googleusercontent.com"))
            // Or, if multiple clients access the backend:
            //.setAudience(Arrays.asList(CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3))
            .setIssuer("accounts.google.com")
            .build();
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         response.setContentType("text/html;charset=UTF-8");

         Connection con = null;
                Statement st = null;
        try{
             con = Util.getConnection();
            st = con.createStatement();
        GoogleIdToken idToken = verifier.verify(request.getParameter("id_token"));
        
        if (idToken == null) {
            idToken = verifier2.verify(request.getParameter("id_token"));
            System.out.println("LOGIN - Verifier 2");
        }
        if (idToken != null) {
            // Success. Return success respone here.
            Payload payload = idToken.getPayload();
            
            // Print user identifier
            String userId = payload.getSubject();
            System.out.println("User ID: " + userId);

            // Get profile information from payload
            String email = payload.getEmail();
            boolean emailVerified = Boolean.valueOf(payload.getEmailVerified());
            String name = (String) payload.get("name");
            String pictureUrl = (String) payload.get("picture");
            String locale = (String) payload.get("locale");
            String familyName = (String) payload.get("family_name");
            String givenName = (String) payload.get("given_name");
            System.out.println("LOGIN - Success");
            if(name==null){name="undefined";}
            try{
                    String query = "insert into register(name,username,email,token_google,status ) "
                            + "values ('" + name + "','" + email + "','" + email + "','" + idToken + "','Verified')";
                    System.out.println(query);
                    int i=0;
                    try{
                     i = st.executeUpdate(query);
                    }catch(Exception e){
                                String message = e.getMessage();
                                
System.out.print(message);
HttpSession session = request.getSession();
                session.setAttribute("username", email);
                session.setAttribute("email", email);
                session.setAttribute("roll", 3);
                 RequestDispatcher rd1 = request.getRequestDispatcher("profile.jsp");
                        rd1.forward(request, response);
                    }System.out.print(12);
                    if (i > 0) {System.out.print(121);
                        HttpSession session = request.getSession();
                session.setAttribute("username", email);
                session.setAttribute("email",email);
                session.setAttribute("roll", 3);
                loginwithgmail n=new loginwithgmail();
                        n.createWallet(email);
                        
                        request.setAttribute("msg", "Registered successfully....");
                        
                        RequestDispatcher rd1 = request.getRequestDispatcher("profile.jsp");
                        rd1.forward(request, response);
                        //  }
                      out.println("{\"Error\": \"False\" ,\"Message\": \"Success\"  ,\"Name\": \""+name+"\",\"Email\": \""+email+"\",\"Avtar\": \"http://papafast.com:8080/MobiTop/ay/images/1.png\"}");

                        
                        
                    } }catch (Exception ex) {
                Logger.getLogger(LoginGoogle.class.getName()).log(Level.SEVERE, null, ex);
                
            }}else {
                        
                        request.setAttribute("msg", "Error....");
                        
                        RequestDispatcher rd1 = request.getRequestDispatcher("profile.jsp");
                        rd1.forward(request, response);
                    }
        }catch (Exception ex) {
                Logger.getLogger(LoginGoogle.class.getName()).log(Level.SEVERE, null, ex);
            }
            //out.println(message);

                //out.println(message);
    }      
                
        
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
