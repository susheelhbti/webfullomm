/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system;
import com.login.util.Util;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author susheel
 */
@WebServlet(name = "CommonFunctions", urlPatterns = {"/CommonFunctions"})
public class CommonFunctions extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }
    
                
        
            
   public int generateOrder(String recharge_number, String recharge_operator, String recharge_circle, Float Amount, String Username) 
            throws SQLException
     
             
    
    {
        try {
            // PrintWriter out=response.getWriter();
            
            
            Connection con = null;
            Statement st = null;
            ResultSet rs;
            
           //HttpSession session = request.getSession();
            //String username = String.valueOf(session.getAttribute("username")).trim();
            
            con = Util.getConnection();
            st = con.createStatement();
            
           
            String q= "insert into transactions(recharge_number,recharge_operator,recharge_circle ,dr,username ,description)"
                 + " values ('"+recharge_number+"','"+recharge_operator+"','"+recharge_circle+"','" +Amount+"','"+Username+"','Recharge of number  " + recharge_number + " amount " + Amount + "' )";
                    System.out.println(q);
                    
                    int i = st.executeUpdate(q, Statement.RETURN_GENERATED_KEYS);
                    
                    System.out.println(q);
                    
                    
                    int insertID = 0 ;
                    rs = st.getGeneratedKeys();
                    System.out.println(q);
                    if (rs.next()){
                        insertID=  rs.getInt(1) ;
                    }
                    
                    
                    return  insertID;
        } catch (Exception ex) {
            Logger.getLogger(CommonFunctions.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    
     public float coupon(String name,float amount) 
            throws SQLException
     
             
    
    {
        try {
            // PrintWriter out=response.getWriter();
            
            
            Connection con = null;
            Statement st = null;
             float discount = 0 ;
           //HttpSession session = request.getSession();
            //String username = String.valueOf(session.getAttribute("username")).trim();
            
            con = Util.getConnection();
            st = con.createStatement();
            
           
            String q="select discount from coupon where name='"+name+"' limit 1" ;
                    System.out.println(q);
                   
                   
                    
                   ResultSet rs1;
            rs1 = st.executeQuery(q);
            if (rs1.next()) {
                 discount = rs1.getFloat("discount");
                    }
           
            float a=(amount*discount)/100;
          float b=amount-a;
           float c = (float) Math.round(b * 100) / 100;
             System.out.print(a); System.out.print(b); System.out.print(c);         
                    
                    return  c;
        } catch (Exception ex) {
            Logger.getLogger(CommonFunctions.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
