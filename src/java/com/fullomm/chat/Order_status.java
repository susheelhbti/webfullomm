/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fullomm.chat;

import com.login.util.Util;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fullomm
 */
public class Order_status extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        PrintWriter out = response.getWriter();
           Connection con = null;
        Statement st = null;

        try {
             HttpSession session = request.getSession();
             String username = String.valueOf(session.getAttribute("username")).trim();
            con = Util.getConnection();
            st = con.createStatement();
            String id = request.getParameter("id");
            String casee = request.getParameter("case");
          
 PreparedStatement stmtt = con.prepareStatement("update bitorder set status=?  where id=? ");

        stmtt.setString(1, casee);

   

        stmtt.setString(2,id);

        // stmtt.setInt(3, insertID);
        int i3 = 0;
        System.out.println("update bitorder set status='"+casee+"'  where id='"+id+"' ");
//address=jsonObj.getString("address");
        try {
            request.setAttribute("msg", "Status  updated successfully!!");
            i3 = stmtt.executeUpdate();
        } catch (SQLException a) {
            a.getMessage();
            System.out.println(a);
        }
             
            response.sendRedirect("TrackOrder.jsp?username="+username);
                     
           
           
                
           
               // request.setAttribute("msg", "Email and Password are Invalid!!");
               // RequestDispatcher rq = request.getRequestDispatcher("index.jsp");
               // rq.forward(request, response);
            

        } catch (Exception e) {
            e.printStackTrace();
        }  
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
