/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fullomm.chat;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.login.util.Util;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jyotiserver2010
 */
public class GetChat extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Connection con = null;
        Statement st = null;
        ResultSet rs;
     //   String username = String.valueOf(request.getParameter("username"));
     //   String Response = String.valueOf(request.getParameter("Response"));
        String id = String.valueOf(request.getParameter("contactId1"));
        System.out.println(43);
        try {
            con = Util.getConnection();
            st = con.createStatement();
            //System.out.println(47);
            PreparedStatement stmt = con.prepareStatement("select contactId1,username,buymessage,time from shoutbox where contactId1="+id+" order by id desc limit 10");
           // System.out.println("select id,username,Response from responsebox where id=4");
            rs = stmt.executeQuery();
            // System.out.println(51);
            ArrayList<GetChat.response> r = new ArrayList<>();
            while (rs.next()) {
                GetChat.response r1 = new GetChat.response();

                r1.setid(rs.getString(1));
                r1.setusername(rs.getString(2));
                r1.setbuymessage(rs.getString(3));
                r1.settime(rs.getString(4));
               r.add(r1);

                System.out.println("submitted");
            }

           Gson gson=new GsonBuilder().create();
    String jsonArray=gson.toJson(r);
            //out.println(messages); 
            out.write(jsonArray);

            //["Hi","Hello","How r u?"]
         //   out.println(messages);
            con.close();
        } catch (Exception e) {
            out.println(e);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
private class response {

        public String r;
        public String id;
        public String buymessage;
        public String username;
        public String time;
        public void setbuymessage(String n) {
            buymessage = n;

        }

        public void setusername(String n) {
            username = n;

        }

        public void setid(String n) {
            id = n;

        }

        public void settime( String n ) {
            time = n;
  
}

    }}
