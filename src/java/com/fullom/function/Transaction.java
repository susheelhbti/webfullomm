/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fullom.function;

import com.login.util.Util;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author fullomm
 */
public class Transaction extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            response.setContentType("text/html;charset=UTF-8");
            String username = request.getParameter("username");
            String bitid = null;
            String bitaddress = null;
            String password = null;
            String msg = null;
            String e = null;
            Connection con = null;
            Statement st = null;
            PrintWriter out = response.getWriter();
            con = Util.getConnection();
            st = con.createStatement();
            String query;
            
            query = "select * from transaction where username='" + username + "' order by id desc  ";

            //out.println(query);
             ResultSet rs1 = st.executeQuery(query);
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Transaction History</title>");            
            out.println("</head>");
            out.println("<body bgcolor=\"#09304D\">"); out.println("<h1>Transaction History</h1>");
            out.println("<table style=\" border: 1px solid black;width:100%;\">");out.println("<thead> <tr>\n" +
"                            <th style=\" border: 1px solid black;width:25%; align=\"center\" bgcolor=\"#828583\">ID</th>\n" +
"                            <th style=\" border: 1px solid black;width:25%; align=\"center\" bgcolor=\"#828583\">amount</th>\n" +
"                            <th style=\" border: 1px solid black;width:25%; align=\"center\" bgcolor=\"#828583\">Receiving address</th>\n" +
"                            <th style=\" border: 1px solid black;width:25%; align=\"center\" bgcolor=\"#828583\">Message</th></tr>\n" +
"                    </thead>");
           
                      

           
            while (rs1.next()) {
                 out.print("<tr>");
                out.print("<td style=\" border: 1px solid black;width:25%;\" align=\"center\"> " + rs1.getString("id") + "</td>");
                System.out.println(3);
                 out.print("<td style=\" border: 1px solid black;width:22%;\" align=\"center\"> " + rs1.getString("amount_in_btc") + "</td>");
                 out.print("<td style=\" border: 1px solid black;width:25%;\" align=\"center\"> " + rs1.getString("address_to") + "</td>");
                 out.print("<td style=\" border: 1px solid black;width:25%; \" align=\"center\"> " + rs1.getString("response_message") + "</td>"); 
             out.print("</tr>");
            }out.println("</table>");
            out.println("</body>");
            out.println("</html>");
        } catch (Exception ex) {
            Logger.getLogger(Transaction.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
