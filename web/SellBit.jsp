<%-- 
    Document   : PendingRecharges
    Created on : Apr 4, 2017, 1:11:18 PM
    Author     : saksham
--%>

<%@page import="com.system.currencyconverter"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html> 

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />

        <title> All System Transactions</title>

        <%@ page import="com.login.util.*" %>
        <%@ page import="java.sql.Connection" %>
        <%@ page import="java.sql.Statement" %>
        <%@ page import="java.sql.ResultSet" %>



        <%@ include file="iheader.jsp" %>



        <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN THEME PANEL -->
            <!-- END THEME PANEL -->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="index.html">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="#">Blank Page</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Page Layouts</span>
                    </li>
                </ul>

            </div>
            <!-- END PAGE BAR -->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title">All System Transactions
                <small></small>
            </h1>
            <!-- END PAGE TITLE-->

            <table id="example"  class="table  table-bordered table-condensed">

                <thead>
                    <tr>	<th>ID</th>
                        <th>Buyer</th>

                        <th>Price/BTC</th>
                        <th>Limits</th>






                        <th>Action</th>



                    </tr>
                </thead>

                <tbody>
                    <%
                        Connection con = null;
                        Statement st = null;

                        try {
                            con = Util.getConnection();
                            st = con.createStatement();
                            String query = "";

                            query = "select * from trade_transaction where type='Buy' order by id desc  ";

                            currencyconverter c = new currencyconverter();
                            double usd = Double.parseDouble(c.cur());

                            System.out.println(usd);

                            //  out.println(query);
                            ResultSet rs = st.executeQuery(query);
                            String status;
                            int transaction_id;
                            while (rs.next()) {
                                double margin = Double.parseDouble(rs.getString("margin"));
                                double price = usd + (margin * usd / 100);
                                System.out.println(price);
                                out.print("<tr>");
                                out.print("<td>" + rs.getString("id") + "</td>");

                                out.print("<td>" + rs.getString("username") + "</td>");
                                out.print("<td>" + price + "</td>");
                                out.print("<td>" + rs.getString("min_transaction") + "--" + rs.getString("max_transcation") + "</td>");

                                // out.print("<td>" + rs.getString("date") + "</td>");
                                String q1 = "<td> <A href='SellBitCoin.jsp?id=" + rs.getString("id") + "'>Sell</a>";

                                out.print(q1);

                                out.print("</tr>");
                            }
                            rs.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            try {
                                if (st != null) {
                                    st.close();
                                }
                                if (con != null) {
                                    con.close();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    %>
                </tbody>	</table>

        </div>


        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <%@ include file="ifooter.jsp" %>

</body>

</html>