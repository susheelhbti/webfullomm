<%-- 
    Document   : PendingRecharges
    Created on : Apr 4, 2017, 1:11:18 PM
    Author     : saksham
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title> All users</title>

        <%@ page import="com.login.util.*" %>
        <%@ page import="java.sql.Connection" %>
        <%@ page import="java.sql.Statement" %>
        <%@ page import="java.sql.ResultSet" %>



        <%@ include file="iheader.jsp" %>
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN THEME PANEL -->
            <!-- END THEME PANEL -->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="index.html">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>

                </ul>

            </div>
            <!-- END PAGE BAR -->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title">All Registered User.....
            </h1>




            <div class="note note-info">
                <center>
                    <font style="color: red; font-weight: bold;">
                    <%                            if (request.getAttribute("msg") != null) {
                            out.print(String.valueOf(request.getAttribute("msg")));
                        }
                    %>
                    </font>
                </center>

                <table id="example"  class="table  table-bordered table-condensed">

                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Balance</th>
                            <th>Status</th>
                            <th>All Address</th>

                        </tr>
                    </thead>

                    <tbody>
                        <%                Connection con = null;
                            Statement st = null;
                                String us = null;

                            try {
                                con = Util.getConnection();
                                st = con.createStatement();
                                String query = "";

                                query = "select * from register   ";

                                ResultSet rs = st.executeQuery(query);
                                String status;
                                int transaction_id;
                                while (rs.next()) {

                                    out.print("<tr>");
                                    out.print("<td>" + rs.getString("id") + "</td>");
us=rs.getString("username") ;
                                    out.print("<td> " + rs.getString("name") + "</td>");
                                    out.print("<td> " + rs.getString("email") + "</td>");
                                    out.print("<td> " + rs.getString("mobile") + "</td>");
                                    out.print("<td> <A ng-click='sub()'>Click here to know balance</a></td>");

                                    
                                    out.print("<td>" + rs.getString("status") + "</td>");
                                   
                                    out.print("<td><A href='useraddress.jsp?username=" + rs.getString("username") + "'>Address</a></td> ");

                                    out.print("</tr>");
                                }
                                rs.close();
                            } catch (Exception e) {
                                e.printStackTrace();
                            } finally {
                                try {
                                    if (st != null) {
                                        st.close();
                                    }
                                    if (con != null) {
                                        con.close();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        %>
                    </tbody>	</table>
{{balance}}
            </div></div></div>

    <%@ include file="ifooter.jsp" %>
 <script>
var app = angular.module("myApp", []);
app.controller("myCtrl", function($scope, $http) {
$scope.showMe = false;
$scope.sub = function() {
 $scope.m="<%out.print(us);%>";
 console.log(111);
 $http.get("BalanceApi?username="+$scope.m)
.then(function(response) {
   $scope.balance = response.data.Balance;
});


};
});
</script> 
</body>

</html>