  
        <% int roll = 0;
            if (session.getAttribute("username") != null && !String.valueOf(session.getAttribute("username")).trim().equalsIgnoreCase("")) {
                response.setHeader("Pragma", "no-cache");
                response.setHeader("Cache-Control", "no-cache");
                response.setDateHeader("Expires", 0);

                //
            } else {
                //  roll =1;
        %>   <jsp:forward page="index.jsp" />
        <%
            }

            String username = String.valueOf(session.getAttribute("username")).trim();
            String rolls = String.valueOf(session.getAttribute("roll")).trim();

            roll = Integer.parseInt(rolls);


        %>         <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content=" " name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" />
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <style>
           .bk:hover{background-color:#F5F7F9}
            </style>
        </head>
    <!-- END HEAD -->
 <body  ng-app="myApp" ng-controller="myCtrl" class="page-header-fixed page-sidebar-closed-hide-logo page-content-white" >
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <div class="page-header navbar navbar-fixed-top" style="background-color:#004A7C;">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner ">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="profile.jsp">
                            <img src="ay/images/t7.png" height="30px" alt="logo" class="logo-default" /> </a>
                        <div class="menu-toggler sidebar-toggler ">
                            <span></span>
                        </div>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                      <ul class="nav navbar-nav pull-right"> 
                          <li >
                                <a href="https://www.dropbox.com/s/vf40orhqx8cbszb/dist.zip?dl=0"  data-close-others="true">
                                    
                                    <span style="color: #FFFFFF">Download </span>
                                    <i  style="color: #FFFFFF" class="fa fa-download"></i>
                                </a>
                                
                            </li>
                          <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" class="img-circle" src="assets/layouts/layout/img/avatar3_small.jpg" />
                                    <span class="username username-hide-on-mobile"> <%= username %> </span>
                                    <i  style="color: #FFFFFF" class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="profile.jsp">
                                            <i class="icon-user"></i> My Profile </a>
                                    </li>
                                   
                                    <li class="divider"> </li>
                                    
                                    <li>
                                        <a href="LogoutServlet">
                                            <i class="icon-key"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                             <li class="dropdown dropdown-quick-sidebar-toggler">
                                <a href="javascript:;" class="dropdown-toggle">
                                    <i class="icon-logout"></i>
                                </a>
                            </li>
                            <!-- END QUICK SIDEBAR TOGGLER -->
                      </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
            </div>
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
          
            <div class="page-container" style=" background-color:#F5F7F9 !important;">
                
                <div class="page-sidebar-wrapper" >
                    <div class="page-sidebar navbar-collapse collapse" style=" background-color:#F5F7F9 !important;" >
                          <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
                            <!-- END SIDEBAR TOGGLER BUTTON -->
                            <!-- DOC: To remove the search box from the sidebar you just need to completeFFFly remove the below "sidebar-search-wrapper" LI element -->
                            <li class="sidebar-search-wrapper">
                                <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                                <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
                                <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
                                <form class="sidebar-search  sidebar-search-bordered" action="#" method="POST">
                                    <a href="javascript:;" class="remove">
                                        <i class="icon-close"></i>
                                    </a>
                                  
                                </form>
                                <!-- END RESPONSIVE QUICK SEARCH FORM -->
                            </li>
                            <%if(roll==10){%>
                            <li class="nav-item start ">
                                <a href="Users.jsp" class="nav-link nav-toggle">
                                    <i class="icon-home"></i>
                                    <span class="title">All Users</span>
                                    <span class="arrow"></span>
                                </a>
                            </li><%}%>
                            <li class="nav-item start ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-home"></i>
                                    <span class="bk title " style=" hover{ background-color: #F5F7F9; }">Payments</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item start ">
                                        <a href="sendbitcoin.jsp" class="nav-link ">
                                            <i class="icon-bar-chart"></i>
                                            <span class="title">Send bitcoin</span>
                                        </a>
                                    </li>
                                    <li class="nav-item start ">
                                        <a href="Receive.jsp" class="nav-link ">
                                            <i class="icon-bulb"></i>
                                            <span class="title">Receive</span>
                                           
                                        </a>
                                    </li>
                                    <li class="nav-item start ">
                                        <a href="ReceiveAddress.jsp" class="nav-link ">
                                            <i class="icon-graph"></i>
                                            <span class="title">AllAddress</span>
                                            
                                        </a>
                                    </li>
                                </ul>
                            </li>
                         
                           <li class="nav-item start ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-home"></i>
                                    <span class="title">Reports</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item start ">
                                        <a href="TransactionsHistory.jsp" class="nav-link ">
                                            <i class="icon-bar-chart"></i>
                                            <span class="title">All Transactions</span>
                                        </a>
                                    </li>
                                     
                                </ul>
                            </li>
                            
                            
                            
                            <li class="nav-item start ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-home"></i>
                                    <span class="title">Security Center</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item start ">
                                        <a href="profile.jsp" class="nav-link ">
                                            <i class="icon-bar-chart"></i>
                                            <span class="title">Profile</span>
                                        </a>
                                    </li>
                                     
                                </ul>
                            </li>
                            
                            
                            
                            
                             <li class="nav-item start ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-home"></i>
                                    <span class="title">Trade</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item start ">
                                        <a href="tradetransaction.jsp" class="nav-link ">
                                            <i class="icon-bar-chart"></i>
                                            <span class="title">Post a trade</span>
                                        </a>
                                    </li>
                                    <li class="nav-item start ">
                                        <a href="SellBit.jsp" class="nav-link ">
                                            <i class="icon-bulb"></i>
                                            <span class="title">Sell</span>
                                           
                                        </a>
                                    </li>
                                    <li class="nav-item start ">
                                        <a href="BuyBit.jsp" class="nav-link ">
                                            <i class="icon-graph"></i>
                                            <span class="title">Buy</span>
                                            
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <!-- END SIDEBAR MENU -->
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->
                </div>
                <!-- END SIDEBAR -->