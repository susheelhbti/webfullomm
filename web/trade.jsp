<%-- 
    Document   : tradetransaction
    Created on : Jul 19, 2017, 1:03:58 PM
    Author     : jyotiserver2010
--%>


<%@page import="org.json.JSONArray"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html> 

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
        <title> All System Transactions</title>

        <%@ page import="com.login.util.*" %>
        <%@ page import="java.sql.Connection" %>
        <%@ page import="java.sql.Statement" %>
        <%@ page import="java.sql.ResultSet" %>



        <%@ include file="iheader.jsp" %>



        <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN THEME PANEL -->
            <!-- END THEME PANEL -->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="index.html">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="#">Post a trade</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span></span>
                    </li>
                </ul>

            </div>
            <!-- END PAGE BAR -->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title">Post a trade
                <small> </small>
            </h1>
            <!-- END PAGE TITLE-->
            <div ng-app="myApp" ng-controller="myCtrl">
                
            
            
          
                <form class="form-horizontal" method="post" action="Trade_Tanscation">
                    <div class="form-group">
                        <label class="col-sm-3">I Want Too..</label>
                        <div class="col-sm-9">
                            <label class="radio">
                                <input type="radio"  name="type"  id="Radio1" value="Sell"> 1(Sell)
                            </label>
                            <label class="radio">
                                <input type="radio" name="type" id="Radio2" value="Buy"> 2(Buy)
                            </label>
                            <label class="radio">
                                <input type="radio" name="type" id="Radio3" value="option3"> 3
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputlocation" class="col-sm-3 control-label">Location</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="inputlocation" name="location" placeholder="Location">
                        </div>
                        
                    </div>
                        <div class="form-group">

                            <label for="inputlocation" class="col-sm-3 control-label">Currency</label>

                            <div class="col-sm-9">
                        <select class="form-control" name="currency"   >
              <option ng-repeat="currency in curr" value="{{currency.name}}">
                {{currency.name}}
              </option>
            </select>
                            </div>
                        </div>

               
                <div class="form-group">

                    <label for="mar" class="col-sm-3 control-label">Margin</label>

                    <div class="col-sm-9">
                        <input type="number" class="form-control" id="mar" name="margin" placeholder="0">   
                    </div>
                    </div>
                    <div class="form-group">
                    <label for="inputnum" class="col-sm-3 control-label">Price Equation</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="inputnum" name="price_equation" placeholder="equation">
                    </div>
                    </div>
                    <div class="form-group">
                    <label for="inputtra" class="col-sm-3 control-label">MIn.Transaction Limit</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control" id="inputtra" name="min_tranaction" placeholder="0">
                    </div>
                    </div>
                    <div class="form-group">
                    <label for="inputtra" class="col-sm-3 control-label">Max.Transaction Limit</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control" id="inputtra"  name="max_tranaction" placeholder="0">
                    </div>
                    </div>
                    <div class="form-group">
                    <label for="inputAmo" class="col-sm-3 control-label">Restricts Amounts To</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control" name="restrict_amount" id="inputAmo" >
                    </div>
                    </div>
                    <div class="form-group">
                    <label for="inputAmo" class="col-sm-3 control-label">Terms Of Trade</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="terms_of_trade" rows="5"></textarea>
                    </div>
                    </div>
                    <div class="form-group">
                    <label for="inputAmo" class="col-sm-3 control-label">Track Liquidity</label> 
                    
                    <div class="col-sm-9">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="track_liquidity" value="Yes">

                            </label>
                            </div>
                        </div>
                    </div>
<div class="form-group">
                    <label for="inputAmo" class="col-sm-3 control-label"> SMS verification required </label> 
                    <div class="col-sm-9">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name=sms_verification" name="" value="Yes">

                            </label>
                        </div>
                        </div>
                    </div>
                    <div class="form-group">
                    <label for="inputAmo" class="col-sm-3 control-label">Trusted People Only</label> 
                    <div class="col-sm-9">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox"  name="trusted_person_only" value="Yes">

                            </label>
                        </div>
                        </div>
                    </div>
                    <br>
                  

                    <input class="btn btn-default " type="submit" value="Publish Advertisement">
               
                    </div>

            </div>
</form>

        </div>
            <!-- END CONTENT -->
            
            <script>
                var app = angular.module('myApp', []);
                app.controller('myCtrl', function ($scope, $http) {
                    $scope.showMe = false;
                    


                        $http.get("Currency")
                                .then(function (response) {
                                    console.log(response);
                                    console.log(response.data,name);
                                   
                                    $scope.curr = response.data;
                                });


                    
                });
            </script> 
            <%@ include file="ifooter.jsp" %></body>

            </html>