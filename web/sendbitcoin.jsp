<%-- 
    Document   : Wallet
    Created on : Jul 15, 2017, 3:55:09 PM
    Author     : jyotiserver2010
--%>

<%@page import="org.json.JSONArray"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html> 

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />

        <title>Send Bitcoin</title>

        <%@ page import="com.login.util.*" %>
        <%@ page import="java.sql.Connection" %>
        <%@ page import="java.sql.Statement" %>
        <%@ page import="java.sql.ResultSet" %>



        <%@ include file="iheader.jsp" %>



        <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN THEME PANEL -->
            <!-- END THEME PANEL -->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="index.html">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="#">Payments</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Send Bitcoin</span>
                    </li>
                </ul>

            </div>
            <!-- END PAGE BAR -->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title">Send Bitcoin
                <small></small>
            </h1>
            <!-- END PAGE TITLE-->

            <div class="row">
                <div class="col-md-6">

                 
                    <p>You can send up to: <strong>0</strong> BTC</p>

                    <form action="SendBitcoin" method="get">

                        <div class="field-row " id="row_id_address_to">



                            <div id="div_id_address_to" class="label-vertical form-group">

                                <label for="id_address_to" class="control-label requiredField">
                                    Receiving bitcoin address
                                </label>


                                <div class="">
                                    <input autocomplete="off" class="form-control" id="id_address_to" name="address_to" placeholder="Bitcoin address" type="text" />
                                </div>
                            </div>

                        </div>
                        <div class="field-row " id="row_id_amount">

                            <div id="div_id_amount" class="label-vertical form-group">

                                <label for="id_amount" class="control-label requiredField">
                                    Amount in bitcoins
                                </label>
                                <div class="">

                                    <input autocomplete="off" class=" form-control" id="id_amount" name="amount" placeholder="0.0000" type="text" value="" />

                                </div>
                            </div>

                        </div> <div style ="display: none; " class="field-row " id="row_id_fees">

                            <div id="div_id_amount" class="label-vertical form-group">

                                <label for="id_amount" class="control-label requiredField">
                                    Fees in bitcoins
                                </label>
                                <div class="">

                                    <input autocomplete="off" class=" form-control" id="id_fees" name="fees" placeholder="0.0000" type="text" value="" />

                                </div>
                            </div>

                        </div>  
                        
                        
                       <!-- 
                        <div class="field-row " id="row_id_fees">

                            <div id="div_id_amount" class="label-vertical form-group">

                                <label for="id_amount" class="control-label requiredField">
                                    Select address from which want to send
                                </label>
                                <div class="">
<%            Connection con = null;
                                Statement st = null;
                                int row=0;
                                 int i=0;
ResultSet rs;String[] uname;
        uname = new String[1000];
                                try {
                                    con = Util.getConnection();
                                    st = con.createStatement();
                                    String query = "";

                                    query = "select address from address  where username='"+username+"' ";

                                     rs = st.executeQuery(query);
                                    String status;
                                    int transaction_id;
                                    while (rs.next()) {
                uname[i++] = rs.getString("address");

            }
                                    rs.close();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        if (st != null) {
                                            st.close();
                                        }
                                        if (con != null) {
                                            con.close();
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                               
                            %>
                                    <select name="from" class=" form-control" >
                                        <%
                            for (int m = 0; m < i; m++) {
                                out.println("<option>" + uname[m] + "</option>");
                            }

                        %>
                                    </select>

                                </div>
                            </div>

                        </div>   
-->



                      
                        <div  style ="display: none; ">  <hr>
                            <h4>More options</h4>
                            <div class="form-group">
                                <div class="label-vertical">
                                    <label>Description</label>
                                </div>
                                <input autocomplete="off" class="form-control" id="id_description" maxlength="100" name="description" placeholder="Appears in the transaction list" type="text" />
                            </div>

                        </div>
                        <hr>
                        <!-- end new -->
                        <button id="sendformBtn" type="submit" name="send_submit" class="btn btn-primary" onclick='$(this).hide();$("#loadingText").show();'>
                            <i class="fa fa-btc"></i> Continue
                        </button>

                    
                        
                    </form>
                    <br>

                  
     </div>
                
    
      <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
              <%@ include file="ifooter.jsp" %>
        
             </body>

</html>