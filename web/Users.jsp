<%-- 
    Document   : Users
    Created on : Jul 28, 2017, 8:11:46 AM
    Author     : fullomm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title> All users</title>

        <%@ page import="com.login.util.*" %>
        <%@ page import="java.sql.Connection" %>
        <%@ page import="java.sql.Statement" %>
        <%@ page import="java.sql.ResultSet" %>



        <%@ include file="iheader.jsp" %>
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN THEME PANEL -->
            <!-- END THEME PANEL -->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="index.html">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>

                </ul>

            </div>
            <!-- END PAGE BAR -->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title">All Registered User.....
            </h1>




            <div class="note note-info">
                <center>
                    <font style="color: red; font-weight: bold;">
                    <%                            if (request.getAttribute("msg") != null) {
                            out.print(String.valueOf(request.getAttribute("msg")));
                        }
                    %>
                    </font>
                </center>

                <table id="example"  class="table  table-bordered table-condensed">

                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Balance</th>
                            <th>Status</th>
                            <th>All Address</th>
                            <th></th>
                            <th>Disable google auth</th>

                        </tr>
                    </thead>

                    <tbody>
             
                  <tr  ng-repeat="x in user" >
                 <td>{{x.id}}</td>
              <td> {{x.name}}</td> 
             <td> {{x.email}}</td>
             <td> {{x.mobile}}</td>
             <td><a ng-show="x.bal != ''">{{x.bal}}</a></td>
             
             <td> {{x.status}}</td>
             <td><A href='useraddress.jsp?username={{x.email}}'>Address</a></td>
             <td> <A ng-click='sub(x)'>Click here to know balance</a></td> 
             <td><A href='disableGoogleauth?username={{x.email}}'>Disable</a></td>
                </tr>
                               

</tbody>
    </table> {{bal}}
       </div></div></div>
  
    <%@ include file="ifooter.jsp" %>
 <script>
var app = angular.module("myApp", []);
app.controller("myCtrl", function($scope, $http) {
           

            //$scope.myTxt = "You have not yet clicked submit";
            $scope.sub = function (xy) {
                 console.log(xy);
             
     $http.get("BalanceApi?username="+xy.email)
                    .then(function (response) {
  console.log(response);
                       xy.bal = response.data.Balance;



         

///////////
                    });

              

};
           $http.get("allUser")
                    .then(function (r) {
 console.log(111);
                        $scope.user = r.data;
 console.log($scope.user);
                    });
        

        });

    </script>