<%-- 
    Document   : addressinfo
    Created on : Jul 27, 2017, 9:27:41 AM
    Author     : fullomm
--%>

<%@page import="com.system.currencyconverter"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html> 

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />

        <title> All System Transactions</title>

        <%@ page import="com.login.util.*" %>
        <%@ page import="java.sql.Connection" %>
        <%@ page import="java.sql.Statement" %>
        <%@ page import="java.sql.ResultSet" %>



        <%@ include file="iheader.jsp" %>



        <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN THEME PANEL -->
            <!-- END THEME PANEL -->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="index.html">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="#">Security Center</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>addressinfo</span>
                    </li>
                </ul>

            </div>
            <!-- END PAGE BAR -->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title">Address Info
                <small>Balance</small>
            </h1>
            <!-- END PAGE TITLE-->




            <%                Connection con = null;
                String hash160 = null;
                String hash1 = null;
                String address_r = null;
                String n_tx = null;
                String total_received = null;
                String total_sent = null;
                Statement st = null;
                String addr=null;
                String script=null;
                int row = 0;
                String amt=null;
                try {
                    con = Util.getConnection();
                    st = con.createStatement();

                    String address = request.getParameter("address");
                    String u = "";
                    // u ="http://127.0.0.1:3000/api/v2/create?password="+ password +"&api_code=321" ;
                    u = "https://blockchain.info/rawaddr/" + address + "";
                    System.out.println(u);
                    currencyconverter c = new currencyconverter();
                    String output = c.wget(u);

                    System.out.println("95");
                    //out.println(output);
                    System.out.println(output);
                    //JSONObject jsonObj = new JSONObject(output);
                    System.out.println("99");

                    //out.println(row);
                    JSONObject root = new JSONObject(output);
                    hash160 = root.getString("hash160");
                    address_r = root.getString("address");
                    n_tx = Integer.toString(root.getInt("n_tx"));
                    total_received = Integer.toString(root.getInt("total_received"));
                    total_sent = Integer.toString(root.getInt("total_sent"));

                    System.out.print("hash160 "+hash160);
                    //out.print(address_r);
                    JSONArray sportsArray = root.getJSONArray("txs");
                    // now get the first element:jar.length()
                    int j = 0;
                    int i = 0;
                    int k=0;
                    for (j = 0; j < sportsArray.length(); j++) {
                       // JSONArray sportsArray1 = root.getJSONArray("txs");
                        JSONObject f = sportsArray.getJSONObject(i);
                         hash1 = f.getString("hash");
                        
                         System.out.print("hash1   "+hash1);
                         JSONArray sportsArray1 = f.getJSONArray("out");
                         JSONArray sportsArray2 = f.getJSONArray("inputs");
                        for (i = 0; i < sportsArray1.length(); i++) {
                           // JSONArray sportsArray1 = root.getJSONArray("out");
                            JSONObject firstSport = sportsArray1.getJSONObject(i);
                            // and so onscript
                            //String a = firstSport.getString("out");
                             addr = firstSport.getString("addr");
                             try{
                             amt = firstSport.getString("value");
                             }
                             catch(Exception e){
                             amt = Integer.toString(firstSport.getInt("value"));
                             }
                            
                            System.out.print(addr+"     addr");
                           
                        }
                        for (i = 0; i < sportsArray2.length(); i++) {
                            JSONObject firstSport1 = sportsArray2.getJSONObject(i);
                            //JSONArray sportsArray3 = firstSport1.getJSONArray("prev_out");
                          
                             JSONObject prev = firstSport1.getJSONObject("prev_out");
                            //String n = firstSport1.getString("prev_out");   
                            
                            // for (k = 0; k < sportsArray3.length(); k++) {
                                 /// JSONObject firstSport2 = sportsArray3.getJSONObject(k);
                             script = prev.getString("script");
                            System.out.print(script+"     script");       
                            // }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (st != null) {
                            st.close();
                        }
                        if (con != null) {
                            con.close();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            %>

            <div class="row-fluid">
                <div class="col-md-5">
                    <table class="table table-striped">
                        <tr>
                            <th colspan="2">Summary</th>
                        </tr>
                        <tr>
                            <td>Address</td>
                            <td><a href="#"><%out.print(address_r);%></a></td>
                        </tr>
                        <tr class="visible-desktop">
                            <td>Hash 160</td>
                            <td><a href="#"><%out.print(hash160);%></a></td>
                        </tr>
                        <tr>
                            <!--<td>Tools</td>
                            <td><a href="/related_tags?active=16pPdjmryVYiQm5U9arH4ZuZ49viPtyXzN">Related Tags</a> - <a href="/unspent?active=16pPdjmryVYiQm5U9arH4ZuZ49viPtyXzN&format=html">Unspent Outputs</a></td>
                            -->  </tr>
                    </table>
                </div>

                <div class="col-md-4" style="overflow:visible;">
                    <table class="table table-striped">
                        <tr>
                            <th colspan="3">Transactions</th>
                        </tr>
                        <tr>
                            <td>No. Transactions</td>
                            <td id="n_transactions"><%out.print(n_tx);%></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Total Received</td>
                            <td id="total_received"><font color="green"><span data-c="<%out.print(total_received);%>"><%out.print(total_received);%></span></font></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Final Balance</td>
                            <td id="final_balance"><font color="green"><span data-c="<%out.print(total_sent);%>"><%out.print(total_sent);%></span></font></td>
                            <td></td>
                        </tr>
                    </table>

                    <!--  <div style="width:420px">
                         <button class="btn" id="create-payment-request">Request Payment</button> <button class="btn" id="create-donation-button">Donation Button</button>
                      </div>-->
                </div>

                <!--https://blockchain.info/qr?data=15FKkpY2rgEMzRg9rDVdPuVX4MPpfiRLVY&size=200
                <div class="col-md-3" align="center">
                    <img src="/qr?data=16pPdjmryVYiQm5U9arH4ZuZ49viPtyXzN&size=200" />
                </div>-->
            </div>

            <div style="clear:both; width:100%; padding-top:20px;">
                <div style="width:100%;margin-bottom:7px;">
                    <h3 style="display:inline">Transactions <!--<small><a href="?sort=0">(Oldest First)</a></small>--></h3>

                    <!--<div style="float:right;">
                        <div class="btn-group" style="display:inline-block">
                            <span class="btn dropdown-toggle primary" data-toggle="dropdown">Filter <span class="caret"></span></span>
                            <ul class="dropdown-menu tx_filter">
                                <li><a href="#" data-value="6"><i class="icon-ok-circle"></i> Unspendable (Default)</a></li>
                                <li><a href="#" data-value="4"><i class="icon-resize-horizontal"></i> All</a></li>
                                <li><a href="#" data-value="1"><i class="icon-arrow-right"></i> Sent</a></li>
                                <li><a href="#" data-value="2"><i class="icon-arrow-left"></i> Received</a></li>
                                <li><a href="#" data-value="5"><i class="icon-check"></i> Confirmed Only</a></li>
                                <li><a href="#" data-value="7"><i class="icon-exclamation-sign"></i> Unconfirmed Only</a></li>
                                <li class="divider"></li>
                                <li><a href="#" data-value="export"> Export History</a></li>
                            </ul>
                        </div>
                    </div>-->
                </div>



               <div id="tx_container" style="width:100%;clear:both">

                  <!--   <div id="tx-265959251" class="txdiv"><table class="table table-striped" cellpadding="0" cellspacing="0" style="padding:0px;float:left;margin:0px;width:100%">
                            <tr><th colspan="3" align="left">
                                    <a class="hash-link" href="/tx/235052990fd300f32771f682ab3e23ee9ea52fe6cdd7719787c35843f86206fb">
                                        235052990fd300f32771f682ab3e23ee9ea52fe6cdd7719787c35843f86206fb</a> <span class="pull-right">
                                            2017-07-06 11:56:51</span></th></tr><tr><td width="500px" class="txtd hidden-phone">16pPdjmryVYiQm5U9arH4ZuZ49viPtyXzN<br />
                                </td><td width="48px" class="hidden-phone" style="padding:4px;text-align:center;vertical-align:middle;"><img src="/Resources/arrow_right_red.png" />
                                </td><td class="txtd"><a href="/address/1Spu5wkiL73oGSH9FHdYKjds95MaH2Fou">1Spu5wkiL73oGSH9FHdYKjds95MaH2Fou</a> <span class="pull-right hidden-phone">
                                        <span data-c="140634" data-time="1499342211000">$ 3.64</span></span><br /><a href="/address/1JTUWwiCj72jmjbWHkJcpPbo6KWCdK8QC2">
                                            1JTUWwiCj72jmjbWHkJcpPbo6KWCdK8QC2</a> <span class="pull-right hidden-phone"><span data-c="386090" data-time="1499342211000">
                                                    
                                                    $ 9.99</span></span><br /></td></tr></table><div style="padding-bottom:30px;width:100%;text-align:right;clear:both"> 
                                                        <button class="btn btn-danger cb"><span data-c="-532374" data-time="1499342211000">$ -13.77</span></button>
                                                    </div>
                    </div>-->

                    <div>
                        <table class="table table-striped" cellpadding="0" cellspacing="0" style="float:left;margin:0;width:100%;padding: 0 0 30px;">
                            <tbody>

                                <tr>
                                    <td class="center">

                                        <div class="bci-ad" data-spot="15" data-host="https://api.blockchain.info">

                                        </div>
                                        <script defer src="/Resources/js/bci-ads.js" type="text/javascript">

                                        </script>

                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                   <div id="tx-265943532" class="txdiv">
                        <table class="table table-striped" cellpadding="0" cellspacing="0" style="padding:0px;float:left;margin:0px;width:100%"><tr><th colspan="3" align="left">
                                    <a class="hash-link" href="#"><%out.print(hash1);%></a> 
                                    <span class="pull-right">2017-07-06 10:22:23</span>
                                </th>
                            </tr>
                            <tr>
                                <td width="500px" class="txtd hidden-phone">
                                    <a href="/address/1B3erbkweuJCNLezAka3r4Zx6UA7NVGJze"><%out.print(script);%></a><br />
                                </td>
                                <td width="48px" class="hidden-phone" style="padding:4px;text-align:center;vertical-align:middle;">
                                    <img src="/Resources/arrow_right_green.png" /></td>
                                <td class="txtd"><%out.print(addr);%>
                                    <span class="pull-right hidden-phone">
                                        <span data-c="532374" data-time="1499336543000">$ 13.77</span>

                                    </span>
                                    <br />
                                </td>
                            </tr>
                        </table>
                        <div style="padding-bottom:30px;width:100%;text-align:right;clear:both"> 
                            <button class="btn btn-success cb"><span data-c="532374" data-time="1499336543000">SAtoshi<%out.print(amt);%></span></button></div></div>





                </div>
            </div>


        </div>

    </div>


    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<%@ include file="ifooter.jsp" %>

</body>

</html>