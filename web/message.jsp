
<%@page import="org.json.JSONArray"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html> 
 
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
      
        <title> All System Transactions</title>

        <%@ page import="com.login.util.*" %>
        <%@ page import="java.sql.Connection" %>
        <%@ page import="java.sql.Statement" %>
        <%@ page import="java.sql.ResultSet" %>



        <%@ include file="iheader.jsp" %>
 
        
    
          <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN THEME PANEL -->
                       <!-- END THEME PANEL -->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">Message</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span></span>
                                </li>
                            </ul>
                            
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title">Message
                            <small> </small>
                        </h1>
            <!-- END PAGE TITLE-->

            <div class="row">
                <div class="col-md-6">

                    <h4>Request submitted... </h4>
                    

            <div class="col-lg-12"> 
 <center id="error_msg" >
     <h2>
            <%
                if (request.getAttribute("msg") != null) {
                    out.print(String.valueOf(request.getAttribute("msg")));
                }
            %>
     </h2>  </center>
            </div>
        
                  
     </div>
                
    
      <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
              <%@ include file="ifooter.jsp" %>
        
             </body>

</html>