<%-- 
    Document   : useraddress
    Created on : Jul 27, 2017, 9:17:11 AM
    Author     : fullomm
--%>

<%@page import="org.json.JSONArray"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html> 

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />

        <title> All System Transactions</title>

        <%@ page import="com.login.util.*" %>
        <%@ page import="java.sql.Connection" %>
        <%@ page import="java.sql.Statement" %>
        <%@ page import="java.sql.ResultSet" %>



        <%@ include file="iheader.jsp" %>



        <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN THEME PANEL -->
            <!-- END THEME PANEL -->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="index.html">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="#">Security Center</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>All Address</span>
                    </li>
                </ul>

            </div>
            <!-- END PAGE BAR -->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title">All Address
                <small>Balance</small>
            </h1>
            <!-- END PAGE TITLE-->



            <table id="example"  class="table  table-bordered table-condensed">

                <thead>
                    <tr>	<th>ID</th>
                      

                        <th>Address</th>
                      
                        <th>Date</th>
                        




                    </tr>
                </thead>

                <tbody>
                    <%                    Connection con = null;
                            Statement st = null;
                           //https://blockchain.info/rawaddr/15FKkpY2rgEMzRg9rDVdPuVX4MPpfiRLVY
                            try {
                                con = Util.getConnection();
                                st = con.createStatement();
                                String query = "";

                                query = " select* from address where username='" + request.getParameter("username") +"' ";

                                System.out.println(query);
                                ResultSet rs = st.executeQuery(query);
                                   int transaction_id;
                                while (rs.next()) {
         System.out.println(query);
                                    out.print("<tr>");
                                     out.print("<td>" + rs.getString("id") + "</td>");
                                     
                                    out.print("<td><a target='_blank' href='https://blockchain.info/address/"+rs.getString("address")+"'  >" + rs.getString("address") + "</a></td>");
                                   
                                    out.print("<td> " + rs.getString("date") + "</td>");

                                    out.print("</tr>");
                                }
                                rs.close();

                            } catch (Exception e) {
                                e.printStackTrace();
                            } finally {
                                try {
                                    if (st != null) {
                                        st.close();
                                    }
                                    if (con != null) {
                                        con.close();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
%>
                </tbody>	</table>


        </div>


        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <%@ include file="ifooter.jsp" %>