<%-- 
    Document   : login
    Created on : May 4, 2017, 2:37:06 PM
    Author     : saksham
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



        <%@ include file="nheader.jsp" %>
 <div class="clearfix"></div>

    
    <section>
        <div class="pagenation-holder">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h3>Admin Login....</h3>
                    </div>

                </div>
            </div>
        </div>
    </section><!--end Section-->
    
    <center id="error_msg" >
     <h2>
            <%
                if (request.getAttribute("msg") != null) {
                    out.print(String.valueOf(request.getAttribute("msg")));
                }
            %>
     </h2>  </center>
    <section class="">
        
    
    <div class="container content">
        <div class="row">
            <div class="col-lg-4">



                <form action="AdminLogin" class="login-form" method="post"> 

                   <div class="row">
                        <div class="col-sm-12 text-right">



                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Username" name="username" required/> 

                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="password" autocomplete="off" placeholder="Password" name="password" required/> 



                            <button class="btn green" type="submit">Sign In</button>
                        </div>
                    </div>
                </form>
            </div>  </div>  </div>    </section><!--end Section-->
     <%@ include file="nfooter.jsp" %> 